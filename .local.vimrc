let g:ctrlp_custom_ignore = {
  \ 'dir':  'target$',
  \ 'file': '\w\+.class$',
  \ }
