package com.macrodatalab.bosrv;

import java.net.URI;
import java.net.URISyntaxException;

import org.apache.thrift.transport.THttpClient;
import org.apache.thrift.protocol.TJSONProtocol;
import org.apache.thrift.protocol.TProtocol;

import org.apache.thrift.transport.TTransportException;

public class Connect
{
    private BigObjectService.Client mClient;
    private String mToken;

    public static Connect obtain(String addr_info, int timeout)
        throws URISyntaxException, TTransportException
    {
        return new Connect(addr_info, timeout);
    }

    public static Connect obtain(String addr_info)
        throws URISyntaxException, TTransportException
    {
        return new Connect(addr_info, 30);
    }

    private Connect(String addr_info, int timeout)
        throws URISyntaxException, TTransportException
    {
        URI url = new URI(addr_info);

        String scheme = url.getScheme().equals("bos") ? "https" : "http";
        int port = url.getPort();

        URI canonical = null;
        if (port != -1)
        {
            canonical = new URI(scheme, "", url.getHost(), url.getPort(), url.getPath(), "", "");
        }
        else
        {
            canonical = new URI(scheme, url.getHost(), url.getPath(), "");
        }

        String[] user_passwd = url.getUserInfo().split(":");
        if (user_passwd != null && user_passwd.length > 0)
        {
            mToken = user_passwd[1];
        }
        else
        {
            mToken = "";
        }

        TProtocol protocol = new TJSONProtocol.Factory().getProtocol(
            new THttpClient(canonical.toString())
        );

        mClient = new BigObjectService.Client(protocol);
    }

    public String getToken()
    {
        return mToken;
    }

    public BigObjectService.Client getClient()
    {
        return mClient;
    }

    public void settimeout(int timeout)
    {
        // NOOP
    }
}
