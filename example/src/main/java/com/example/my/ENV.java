import com.macrodatalab.bosrv.*;
import com.macrodatalab.exc.*;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import org.apache.commons.lang3.StringUtils;
import org.apache.thrift.TException;

class ENV
{
    public static void setup_environ(String bourl)
        throws URISyntaxException, ServiceError, SQLError, AuthError, TException
    {
        Connect conn = Connect.obtain(bourl);

        String token = conn.getToken();
        BigObjectService.Client client = conn.getClient();

        String[] sql_stmt = {
            "CREATE TABLE users (id INT, gender STRING, country STRING, KEY (id))",
            "CREATE TABLE shoes (id INT, brand STRING, KEY(id))",
            "CREATE TABLE steps (users.id INT, shoes.id INT, FACT step INT)"
        };

        for (String create_stmt : sql_stmt)
        {
            client.execute(token, create_stmt, "", "");
        }

        String insert_stmt = null;

        insert_stmt = "INSERT INTO users VALUES " + StringUtils.join(USER.sList, " ");
        client.execute(token, insert_stmt, "", "");

        insert_stmt = "INSERT INTO shoes VALUES " + StringUtils.join(SHOE.sList, " ");
        client.execute(token, insert_stmt, "", "");
    }

    public static class step
    {
        public int mUid;
        public int mSid;
        public int mStep;

        public step(int uid, int sid, int step)
        {
            mUid = uid;
            mSid = sid;
            mStep = step;
        }

        public String toString()
        {
            return String.format("(%d, %d, %d)", mUid, mSid, mStep);
        }
    }

    public static void populate_steps(String bourl, boolean report, int count)
        throws URISyntaxException, ServiceError, SQLError, AuthError, TException
    {
        Random rand = new Random();

        Connect conn = Connect.obtain(bourl);

        String token = conn.getToken();
        BigObjectService.Client client = conn.getClient();

        long now, end;

        now = new Date().getTime();
        for (int cnt = 0; cnt < count; cnt++)
        {
            ArrayList<step> step_grp_val = new ArrayList<step>();
            String insert_stmt = "";

            for (int r = 0; r < 10000; r++)
            {
                step_grp_val.add(new step(
                    rand.nextInt(USER.sList.length) + 1,
                    rand.nextInt(SHOE.sList.length) + 1,
                    rand.nextInt(10) + 1
                ));
            }

            insert_stmt = "INSERT INTO steps VALUES " + StringUtils.join(step_grp_val, " ");
            client.execute(token, insert_stmt, "", "");
        }
        end = new Date().getTime();

        if (report)
        {
            System.out.println("--------------------------");
            System.out.format("Insert time for %d values: %f\n", 10000 * count, (end - now) / 1000.f);
            System.out.println("--------------------------\n");
        }
    }
}
