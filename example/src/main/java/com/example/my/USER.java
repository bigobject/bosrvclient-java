class USER
{
    public int mId;
    public String mGender;
    public String mLoc;

    public USER(int id, String gender, String loc)
    {
        mId = id;
        mGender = gender;
        mLoc = loc;
    }

    public String toString()
    {
        return String.format("(%d, '%s', '%s')", mId, mGender, mLoc);
    }

    public static final USER[] sList = {
        new USER(1, "M", "Taiwan"),
        new USER(2, "M", "US"),
        new USER(3, "F", "Sleepy Hollow"),
        new USER(4, "F", "Taiwan"),
        new USER(5, "M", "Japan"),
        new USER(6, "F", "United Kindom"),
        new USER(7, "F", "Scott"),
        new USER(8, "F", "Happy Tree Valley")
    };
}
