class SHOE
{
    public int mId;
    public String mBrand;

    public SHOE(int id, String brand)
    {
        mId = id;
        mBrand = brand;
    }

    public String toString()
    {
        return String.format("(%d, '%s')", mId, mBrand);
    }

    public static final SHOE[] sList = {
        new SHOE(1, "Toms"),
        new SHOE(2, "Sperry"),
        new SHOE(3, "Kate"),
        new SHOE(4, "Sofi"),
        new SHOE(5, "McClain"),
        new SHOE(6, "Cherry"),
        new SHOE(7, "Miu Miu"),
        new SHOE(8, "Salvatore Ferrogamo")
    };
}
