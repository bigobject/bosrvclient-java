import com.macrodatalab.bosrv.*;
import com.macrodatalab.exc.*;

import java.util.Date;

import org.json.*;

class SqlClient
{
    public static void main(String [] args)
    {
        String bourl = System.getenv("BIGOBJECT_URL");

        long now, end;
        try
        {
            ENV.setup_environ(bourl);
            ENV.populate_steps(bourl, true, 1);

            Connect conn = Connect.obtain(bourl);

            String token = conn.getToken();
            BigObjectService.Client client = conn.getClient();

            String select_stmt = "SELECT SUM(step) FROM steps GROUP BY users.gender, shoes.brand";
            now = new Date().getTime();
            String table = client.execute(token, select_stmt, "", "");
            end = new Date().getTime();

            System.out.println("--------------------------");
            System.out.println(select_stmt);
            System.out.format("Operation took time: %f\n", (end - now) / 1000.f);
            System.out.println("--------------------------\n");

            JSONArray result_table = new JSONArray();
            RangeSpec rng = new RangeSpec();
            rng.page = 100;
            int eol = 0;

            now = new Date().getTime();
            while (eol != -1)
            {
                JSONArray data = new JSONArray(client.cursor_fetch(
                    token,
                    table,
                    rng
                ));
                eol = data.getInt(0);
                JSONArray rows = data.getJSONArray(1);
                for (int r = 0; r < rows.length(); r++)
                {
                    result_table.put(rows.getJSONArray(r));
                }
            }
            end = new Date().getTime();

            System.out.println("--------------------------");
            System.out.format("Time spent to retrieve from cursor: %f\n", (end - now) / 1000.f);
            for (int r = 0; r < result_table.length(); r++)
            {
                JSONArray oneRow = result_table.getJSONArray(r);
                System.out.println("[" + oneRow.join(", ") + "]");
            }
            System.out.println("--------------------------\n");
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            System.exit(1);
        }
    }
}
